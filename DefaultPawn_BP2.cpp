// Fill out your copyright notice in the Description page of Project Settings.


#include "DefaultPawn_BP2.h"

// Sets default values
ADefaultPawn_BP2::ADefaultPawn_BP2()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ADefaultPawn_BP2::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ADefaultPawn_BP2::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ADefaultPawn_BP2::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

