// Fill out your copyright notice in the Description page of Project Settings.

#include "Grabber.h"
#include "DrawDebugHelpers.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"
#include "AITestSuite/Public/AITestsCommon.h"

// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;	
}


void UGrabber::SetupInputComponent()
{
	InputComponent = GetOwner()->FindComponentByClass<UInputComponent>();
	if (InputComponent) {
		UE_LOG(LogTemp, Warning, TEXT("Input component found"), *GetOwner()->GetName());
		InputComponent->BindAction("Grab", IE_Pressed, this, &UGrabber::Grab);
		InputComponent->BindAction("Grab", IE_Released, this, &UGrabber::GrabReleased);
	}

}

// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();
	// UE_LOG(LogTemp, Warning, TEXT("Executed"));
	FindPhysicsHandle();
	SetupInputComponent;
	//UInputComponent InputComponent;
	
}

void UGrabber::FindPhysicsHandle()
{	
	// ...
	PhysicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
	if (!PhysicsHandle)
	{
		UE_LOG(LogTemp, Error, TEXT("Found input component %s"), *GetOwner()->GetName());
	}
	
}

void UGrabber::GrabReleased()
{
	if (!PhysicsHandle) { return; }
	PhysicsHandle->ReleaseComponent();
	//UPhysicsHandleComponent();
}

void UGrabber::Grab()
{
	
FHitResult HitResult = GetFirstPhysicsBodyInReach();	
UPrimitiveComponent* ComponentToGrab = HitResult.GetComponent();	
AActor* ActorHit = HitResult.GetActor();
	
	if (ActorHit)
	{
		if (!PhysicsHandle) { return; }
		PhysicsHandle->GrabComponentAtLocation(ComponentToGrab,	NAME_None, GetPlayersReach());

	}
}


// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);	
	if (!PhysicsHandle) { return; }
	if(PhysicsHandle->GrabbedComponent)
	{
		PhysicsHandle->SetTargetLocation(GetPlayersReach());
	}
	
	//FVector PlayerViewPointLocation;
	//FRotator PlayerViewPointRotation;
//	GetFirstPhysicsBodyInReach();
	//GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(a, b);
	//UE_LOG(LogTemp, Warning, TEXT("Location %s, Rotation %s"),
	//	*a.ToString(),
	//	*b.ToString()); // cant use ToString on UE_LOG ......
	
	// draw line from player showing reach
	// FVector LineTraceEnd = PlayerViewPointLocation + PlayerViewPointRotation.Vector() * Reach;

	
	
	
}

FHitResult UGrabber::GetFirstPhysicsBodyInReach() const
{
	FHitResult Hit;
	
	FCollisionQueryParams TraceParams(FName(TEXT("")), false, GetOwner());

	GetWorld()->LineTraceSingleByObjectType(
		Hit,
		GetPlayersWorldPos(),
		GetPlayersReach(),
		FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody),
		TraceParams
	);
	return Hit;
}

FVector UGrabber::GetPlayersWorldPos() const
{
	FVector PlayerViewPointLocation;
	FRotator PlayerViewPointRotation;
	//FVector LineTraceEnd = PlayerViewPointLocation
		return PlayerViewPointLocation;
}

FVector UGrabber::GetPlayersReach() const
{
	FVector PlayerViewPointLocation;
	FRotator PlayerViewPointRotation;
	return PlayerViewPointLocation + FVector(0.f, 0.f, 100.f);	
}



//void line_tracing_stuff()
//{
//	FVector PlayerViewPointLocation;
//	FRotator PlayerViewPointRotation;
//	FVector LineTraceEnd = PlayerViewPointLocation + FVector(0.f, 0.f, 100.f);
//	/*LineTraceEnd = (LineTraceEnd * Reach) + PlayerViewPointLocation;*/
//
//	GetWorld()->LineTraceSingleByObjectType(
//		Hit,
//		PlayerViewPointLocation,
//		LineTraceEnd,
//		FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody),
//		TraceParams
//	);
//
//	
//	UE_LOG(LogTemp, Warning, TEXT("Executed line"));
//
//	DrawDebugLine(
//		FAITestHelpers::GetWorld(),
//		PlayerViewPointLocation,
//		LineTraceEnd,
//		FColor(0, 255, 0),
//		false,
//		0.f,
//		0.f,
//		5.f
//	);

