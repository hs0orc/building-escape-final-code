// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "building_escape.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, building_escape, "building_escape" );
