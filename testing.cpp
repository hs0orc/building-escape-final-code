// Fill out your copyright notice in the Description page of Project Settings.


#include "testing.h"
#include "Components/AudioComponent.h"
#include "Components/PrimitiveComponent.h"
#include "GameFramework/Actor.h"
#include "CoreMinimal.h"
#include "Math/Rotator.h"
#include "GameFramework/PlayerController.h"
#include "Engine/World.h"
#define OUT

// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();
	
	InitialYaw = GetOwner()->GetActorRotation().Yaw;
	CurrentYaw = InitialYaw;
	TargetYaw += InitialYaw;
	FindPressurePlate();
	FindAudioComponent();
	
}

void UOpenDoor::FindPressurePlate()
{
	if (!PressurePlate)
	{
		UE_LOG(LogTemp, Error, TEXT("%s has open door comp on it but no presure plate"), *GetOwner()->GetName())
			//if(GetOwner()->GetActorLabel())
	}

}

void UOpenDoor::FindAudioComponent()
{
	AudioComponent = GetOwner()->FindComponentByClass<UAudioComponent>();
	if (!AudioComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("%s missing audio component"), *GetOwner()->GetName());
	}
}


void UOpenDoor::OpenDoor(float DeltaTime)
{
	CurrentYaw = FMath::Lerp(CurrentYaw, OpenAngle, DeltaTime * DoorOpenSpeed);
	FRotator DoorRotation = GetOwner()->GetActorRotation();
	DoorRotation.Yaw = CurrentYaw;
	GetOwner()->SetActorRotation(DoorRotation);
	/*float CurrentYaw = GetOwner()->GetActorRotation().Yaw;
	FRotator something_doice(0.f, TargetYaw, 0.f);
	something_doice.Yaw = FMath::Lerp(CurrentYaw, TargetYaw, DeltaTime * 1.f);
	GetOwner()->SetActorRotation(something_doice);*/
	
	CloseDoorSound = false;
	if (!AudioComponent) { return; }
	if (!OpenDoorSound)
	{
		AudioComponent->Play();
		OpenDoorSound = true;
	}
	
	//GetOwner()->GetName();

}

void UOpenDoor::CloseDoor(float DeltaTime)
{

	//float CurrentYaw = GetOwner()->GetActorRotation().Yaw;

	//FRotator DoorRotation(0.f, TargetYaw, 0.f);
	CurrentYaw = FMath::Lerp(CurrentYaw, InitialYaw, DeltaTime * 2.f);
	FRotator DoorRotation = GetOwner()->GetActorRotation();
	DoorRotation.Yaw = CurrentYaw;
	GetOwner()->SetActorRotation(DoorRotation);

	OpenDoorSound = false;
	if (!AudioComponent) { return; }
	
	if (!CloseDoorSound)
	{
		AudioComponent->Play();
		CloseDoorSound = true;
	}
	
	
	//GetOwner()->GetName();
	/*float CurrentYaw = GetOwner()->GetActorRotation().Yaw;
	FRotator something_doice(0.f, TargetYaw, 0.f);
	something_doice.Yaw = FMath::Lerp(CurrentYaw, TargetYaw, DeltaTime * 1.f);
	GetOwner()->SetActorRotation(something_doice);*/
}

// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	/*UE_LOG(LogTemp, Warning, TEXT("%s"), *GetOwner()->GetActorRotation().ToString());
	UE_LOG(LogTemp, Warning, TEXT("target yaw is %f"), GetOwner()->GetActorRotation().Yaw);*/

	/*float CurrentYaw = GetOwner()->GetActorRotation().Yaw;
	FRotator something_doice(0.f, TargetYaw, 0.f);
	something_doice.Yaw = FMath::Lerp(CurrentYaw, TargetYaw, DeltaTime * 1.f);
	GetOwner()->SetActorRotation(something_doice);*/
	if (TotalMassOfActors() > MassToOpenDoors)
	{
		OpenDoor(DeltaTime);
		DoorLastOpened = GetWorld()->GetTimeSeconds();
	
	}
	else {
		
		if (GetWorld()->GetTimeSeconds() - DoorLastOpened > DoorCloseDelay) {
			CloseDoor(DeltaTime);

		}
	}
	//GetWorld()->GetTimeSeconds();
	
	//GetOwner()->SetActorE();
	
	// ...
}

float UOpenDoor::TotalMassOfActors() const
{
	float TotalMass = 0.f;
	
	TArray<AActor*> OverLappingActors;

	if (!PressurePlate) { return TotalMass; }

	PressurePlate->GetOverlappingActors(OUT OverLappingActors);	
	
	for (AActor* Actor : OverLappingActors)
	{
		
		TotalMass += TotalMass + Actor->FindComponentByClass<UPrimitiveComponent>()->GetMass();
		UE_LOG(LogTemp, Warning, TEXT("%s is on the pressureplate"), *Actor->GetName());
	}
	
	return TotalMass;
}
