// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/AudioComponent.h"
#include "Components/ActorComponent.h"
#include "Components/InputComponent.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "Grabber.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BUILDING_ESCAPE_API UGrabber : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGrabber();

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	
protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	
private:
	float Reach = 100.f;

	UPROPERTY()	
	UPhysicsHandleComponent* PhysicsHandle = nullptr;
	UPROPERTY()
	UInputComponent* InputComponent = nullptr;
	
	void UGrabber::FindPhysicsHandle();
	void Grab();
	void UGrabber::GrabReleased();
	void SetupInputComponent();
	void line_tracing_stuff();

	FHitResult GetFirstPhysicsBodyInReach() const;

	FVector GetPlayersReach() const;
	FVector GetPlayersWorldPos() const;


};
